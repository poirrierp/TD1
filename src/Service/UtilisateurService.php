<?php
namespace TheFeed\Service;

use Symfony\Component\HttpFoundation\File\Stream;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryI;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceI
{
    private UtilisateurRepositoryI $user;
    private String $dossierPhotoDeProfil;
    private FileMovingServiceInterface $movefile;

    /**
     * @param UtilisateurRepositoryI $user
     */
    public function __construct(UtilisateurRepositoryI $user, String $photo, FileMovingServiceInterface $file)
    {
        $this->movefile = $file;
        $this->dossierPhotoDeProfil = $photo;
        $this->user = $user;
    }


    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil): void
    {
        if (
            isset($login) && isset($motDePasse) && isset($adresseMail)
            && isset($nomPhotoDeProfil)
        ) {
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException( "Le login doit être compris entre 4 et 20 caractères!");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException( "Mot de passe invalide!");
            }
            if (!filter_var($adresseMail, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException( "L'adresse mail est incorrecte!");
            }


            $utilisateur = $this->user->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!" );
            }

            $utilisateur = $this->user->recupererParEmail($adresseMail);
            if ($utilisateur != null) {
                throw new ServiceException( "Un compte est déjà enregistré avec cette adresse mail!");
            }

            $mdpHache = MotDePasse::hacher($motDePasse);

            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $nomPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException( "La photo de profil n'est pas au bon format!");
            }
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $nomPhotoDeProfil['tmp_name'];
            $to = "$this->dossierPhotoDeProfil/$pictureName";
            $this->movefile->moveFile($from, $to);

            $utilisateur = Utilisateur::create($login, $mdpHache, $adresseMail, $pictureName);
            $this->user->ajouter($utilisateur);
        } else {
            throw new ServiceException( "Login, nom, prenom ou mot de passe manquant.");
        }
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) : ?Utilisateur{
        $utilisateur = $this->user->recupererParClePrimaire($idUtilisateur);
        if(!$autoriserNull && $utilisateur===null) {
            throw new ServiceException( "Login inconnu.", Response::HTTP_UNAUTHORIZED);
        }
        return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function connection($login, $mdp): void{
        if (!($login) && isset($mdp)){
            throw new ServiceException( "Login ou mot de passe manquant.");
        }

        /** @var Utilisateur $utilisateur */
        $utilisateur = $this->user->recupererParLogin($login);

        if ($utilisateur === null) {
            throw new ServiceException( "Login inconnu.");
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException( "Mot de passe incorrect.");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    public function deconnecter(): void{
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException( "Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }
}
