<?php
namespace TheFeed\Service;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryI;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Modele\Repository\UtilisateurRepositoryI;
use TheFeed\Service\Exception\ServiceException;

class PublicationsService implements PublicationsServiceI
{
    private PublicationRepositoryI $repo;
    private UtilisateurRepositoryI $user;

    /**
     * @param PublicationRepositoryI $repo
     */
    public function __construct(PublicationRepositoryI $repo, UtilisateurRepositoryI $u)
    {
        $this->repo = $repo;
        $this->user = $u;
    }


    public function recupererPublications(): array
    {
        return $this->repo->recuperer();
    }

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, $message): Publication
    {
        if ($idUtilisateur == null) throw new ServiceException("Il faut être connecté pour publier un feed", Response::HTTP_UNAUTHORIZED);
        if ($message == null || $message == "") throw new ServiceException("Le message ne peut pas être vide!", Response::HTTP_BAD_REQUEST);
        if (strlen($message) > 250) throw new ServiceException("Le message ne peut pas dépasser 250 caractères!", Response::HTTP_BAD_REQUEST);

        $auteur = new Utilisateur();
        $auteur->setIdUtilisateur($idUtilisateur);
        $publication = Publication::create($message, $auteur);
        $idPublication = $this->repo->ajouter($publication);
        $publication->setIdPublication($idPublication);
        return $publication;
    }

    public function recupererPublicationsUtilisateur($idUtilisateur): array{
        if($idUtilisateur===null){
            throw new ServiceException("Utilisateur inexistant.");
        }
        return $this->repo->recupererParAuteur($idUtilisateur);
    }

    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void
    {
        $publication = $this->repo->recupererParClePrimaire($idPublication);

        if (is_null($idUtilisateurConnecte))
            throw new ServiceException("Il faut être connecté pour supprimer une publication", Response::HTTP_UNAUTHORIZED);

        if ($publication === null)
            throw new ServiceException("Publication inconnue.", Response::HTTP_NOT_FOUND);

        if ($publication->getAuteur()->getIdUtilisateur() !== intval($idUtilisateurConnecte))
            throw new ServiceException("Seul l'auteur de la publication peut la supprimer", Response::HTTP_FORBIDDEN);

        $this->repo->supprimer($publication);
    }

    /**
     * @throws ServiceException
     */
    public function recupererPublicationParId($idPublication, $autoriserNull = true) : ?Publication {
        $publication = $this->repo->recupererParClePrimaire($idPublication);
        if(!$autoriserNull && $publication == null) {
            throw new ServiceException("La publication n'existe pas.", Response::HTTP_NOT_FOUND);
        }
        return $publication;
    }

}

