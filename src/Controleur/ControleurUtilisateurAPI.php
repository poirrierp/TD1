<?php
namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\UtilisateurServiceI;

class ControleurUtilisateurAPI extends ControleurGenerique
{

    public function __construct (
        ContainerInterface $container,
        private readonly UtilisateurServiceI $utilisateurService
    )
    {
        parent::__construct($container);
    }

    #[Route(path: '/api/utilisateurs/{idUtilisateur}', name:'afficherDetailUtilisateurApi', methods:["GET"])]
    public function afficherDetail($idUtilisateur): Response{
        try {
            return new JsonResponse($this->utilisateurService->recupererUtilisateurParId($idUtilisateur),Response::HTTP_UNAUTHORIZED);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }


}