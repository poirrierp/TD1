<?php

namespace TheFeed\Controleur;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationsService;
use TheFeed\Service\PublicationsServiceI;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique
{

    /**
     * @param PublicationsServiceI $p
     */
    public function __construct(ContainerInterface $container, private PublicationsServiceI $p)
    {
        parent::__construct($container);
    }

    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    #[Route(path: '/', name:'afficherListe')]
    public function afficherListe(): Response
    {
        $publications = $this->p->recupererPublications();
        return $this->afficherTwig('publication/feed.html.twig', [
            "pagetitle" => "Publications",
            "publications" => $publications,
            "cheminVueBody" => "base.html.twig",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/publications', name:'Liste', methods:["POST"])]
    public function creerDepuisFormulaire(): RedirectResponse
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try{
            $this->p->creerPublication($idUtilisateurConnecte,$message);
        }
        catch (ServiceException $e){
            MessageFlash::ajouter("error",$e->getMessage());
            return ControleurPublication::rediriger('afficherListe');
        }
        MessageFlash::ajouter("success","Création réusssi");
        return ControleurPublication::rediriger('afficherListe');
    }


}