<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use TheFeed\Controleur\ControleurUtilisateur;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationsService;
use TheFeed\Service\UtilisateurService;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


class RouteurURL
{
    public static function traiterRequete(Request $requete): Response {
        $conteneur = new ContainerBuilder();
        $conteneur->setParameter('project_root', __DIR__.'/../..');
        $conteneur->set('container', $conteneur);



        //On indique au FileLocator de chercher à partir du dossier de configuration
        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__."/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");

        $twigLoader = new FilesystemLoader(__DIR__ . '/../vue/');
        $twig=$conteneur->get('twig');

        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);


        $conteneur->set('routes', $routes);


        $contexteRequete = (new RequestContext())->fromRequest($requete);

        $conteneur->set('request_context', $contexteRequete);

        $generateurUrl=$conteneur->get('url_generator');
        $assistantUrl=$conteneur->get('url_helper');
        $twig->addFunction(new TwigFunction("route", function ($nomRoute, $parametres = []) use ($generateurUrl) {return $generateurUrl->generate($nomRoute, $parametres);}));
        $twig->addFunction(new TwigFunction("asset", function ($chemin) use ($assistantUrl) {return $assistantUrl->getAbsoluteUrl($chemin);}));
        $twig->addGlobal('utilisateur',ConnexionUtilisateur::getIdUtilisateurConnecte());
        $twig->addGlobal('messagesFlash', new MessageFlash());

        try{
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());

            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);
            $response = call_user_func_array($controleur, $arguments);
        } catch (MethodNotAllowedException $exception) {
            $response = ($conteneur->get('controleur_generique'))->afficherErreur($exception->getMessage(), 405);
        } catch (ResourceNotFoundException $exception) {
            $response = ($conteneur->get('controleur_generique'))->afficherErreur($exception->getMessage(), 404);
        } catch (\Exception $exception) {
            $response = ($conteneur->get('controleur_generique'))->afficherErreur($exception->getMessage()) ;
        }
        return $response;
    }
}