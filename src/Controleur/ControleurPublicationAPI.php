<?php
namespace TheFeed\Controleur;

use JsonException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Service\PublicationsServiceI;
use Symfony\Component\HttpFoundation\Request;

class ControleurPublicationAPI extends ControleurGenerique
{

    public function __construct (
        ContainerInterface $container,
        private readonly PublicationsServiceI $publicationService
    )
    {
        parent::__construct($container);
    }

    #[Route(path: '/api/publications/{idPublication}', name:'supprimerPublicationApi', methods:["DELETE"])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse($this->afficherDetail($idPublication), Response::HTTP_NO_CONTENT);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications/{idPublication}', name:'afficherDetailPublicationApi', methods:["GET"])]
    public function afficherDetail($idPublication): Response{
        try {
            return new JsonResponse($this->publicationService->recupererPublicationParId($idPublication),Response::HTTP_NOT_FOUND);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'afficherListePublicationApi', methods:["GET"])]
    public function afficherListe(): Response{
        try {
            return new JsonResponse($this->publicationService->recupererPublications(),Response::HTTP_NOT_FOUND);
        }
        catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }

    #[Route(path: '/api/publications', name:'posterPublicationPublicationApi', methods:["POST"])]
    public function posterPublication(Request $request): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $data = json_decode($request->getContent(), flags: JSON_THROW_ON_ERROR);
            $message = $data->message ?? null;

            $publication = $this->publicationService->creerPublication($idUtilisateurConnecte, $message);
            return new JsonResponse($publication, Response::HTTP_CREATED);
        } catch (JsonException $exception) {
            return new JsonResponse(
                ["error" => "Corps de la requête mal formé"],
                Response::HTTP_BAD_REQUEST
            );
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }


}