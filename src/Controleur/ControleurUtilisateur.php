<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationsService;
use TheFeed\Service\PublicationsServiceI;
use TheFeed\Service\UtilisateurServiceI;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurUtilisateur extends ControleurGenerique
{

    /**
     * @param PublicationsServiceI $ps
     * @param UtilisateurServiceI $us
     */
    public function __construct(ContainerInterface $container, private PublicationsServiceI $ps, private UtilisateurServiceI $us)
    {
        parent::__construct($container);
    }


    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications')]
    public function afficherPublications($idUtilisateur): Response
    {
        try{
            $utilisateur = $this->us->recupererUtilisateurParId($idUtilisateur);
        }
        catch (ServiceException $e){
            MessageFlash::ajouter("error",$e->getMessage());
            return ControleurUtilisateur::rediriger("afficherListe",[]);
        }
        $loginHTML = htmlspecialchars($utilisateur->getLogin());
        $publications = $this->ps->recupererPublicationsUtilisateur($idUtilisateur);
        return $this->afficherTwig('publication/page_perso.html.twig', [
            "publications" => $publications,
            "pagetitle" => "Page de $loginHTML",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/inscription', name:'afficherFormulaireCreation', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig('utilisateur/inscription.html.twig', [
            "pagetitle" => "Création d'un utilisateur",
            "cheminVueBody" => "base.html.twig",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/inscription', name:'inscription', methods:["POST"])]
    public function creerDepuisFormulaire(): RedirectResponse
    {
            $login = $_POST['login']??null;
            $motDePasse = $_POST['mot-de-passe']??null;
            $adresseMail = $_POST['email']??null;
            $nomPhotoDeProfil = $_FILES['nom-photo-de-profil']??null;
            try{
                $this->us->creerUtilisateur($login,$motDePasse,$adresseMail,$nomPhotoDeProfil);
            }
            catch (ServiceException $e){
                MessageFlash::ajouter("error",$e->getMessage());
                return $this->rediriger('inscription');
            }
            MessageFlash::ajouter("success","l'utilisateur à bien été créé!");
            return $this->rediriger('afficherListe');
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherTwig('utilisateur/connexion.html.twig', [
            "pagetitle" => "Formulaire de connexion",
            "cheminVueBody" => "base.html.twig",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
    }

    #[Route(path: '/connexion', name:'connecter', methods:["POST"])]
    public function connecter(): RedirectResponse
    {
        try{
            $this->us->connection($_POST['login'],$_POST['mot-de-passe']);
        }
        catch (ServiceException $e){
            MessageFlash::ajouter("error", $e->getMessage());
            return ControleurUtilisateur::rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return ControleurUtilisateur::rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name:'deconnexion', methods:["GET"])]
    public function deconnecter(): RedirectResponse
    {
        try{
            $this->us->deconnecter();
        }
        catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return ControleurUtilisateur::rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return ControleurUtilisateur::rediriger("afficherListe");
    }
}
