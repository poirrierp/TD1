<?php

namespace TheFeed\Modele\Repository;

use TheFeed\Configuration\Configuration;
use TheFeed\Configuration\ConfigurationBDDInterface;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use PDO;

class ConnexionBaseDeDonnees implements ConnexionBaseDeDonneesI
{
    private PDO $pdo;

    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    public function __construct(ConfigurationBDDInterface $configurationBDD)
    {
        // Connexion à la base de données
        $this->pdo = new PDO(
            $configurationBDD->getDSN(),
            $configurationBDD->getLogin(),
            $configurationBDD->getMotDePasse(),
            $configurationBDD->getOptions()
        );

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}

}