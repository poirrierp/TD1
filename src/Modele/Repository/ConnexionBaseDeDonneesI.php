<?php

namespace TheFeed\Modele\Repository;

use PDO;

interface ConnexionBaseDeDonneesI
{
    public function getPdo(): PDO;
}