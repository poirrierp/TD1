<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\Type\Parameter;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryI as PublicationRepositoryInterface ;
use TheFeed\Modele\Repository\UtilisateurRepositoryI as UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationsService as PublicationService;
use function PHPUnit\Framework\assertEquals;

class PublicationServiceTest extends TestCase
{
    private $service;

    private $publicationRepositoryMock;

    private $utilisateurRepositoryMock;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }


    public function testCreerPublicationUtilisateurInexistant(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->service->creerPublication(-2,"test");
    }

    /**
     * @throws Exception
     */
    public function testCreerPublicationVide (){
        $this->expectException(ServiceException::class);
        $u = $this->createMock(Utilisateur::class);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->withAnyParameters()->willReturn($u);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->service->creerPublication(1,"");
    }

    /**
     * @throws ServiceException
     */
    public function testCreerPublicationValide (){
        $fakePublications = [new Publication(), new Publication()];
        $u = $this->createMock(Utilisateur::class);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->withAnyParameters()->willReturn($u);
        $this->publicationRepositoryMock->method("ajouter")->willReturnCallback(function ($p){
            assertEquals("toto",$p->getMessage());
        });
        $this->service->creerPublication(1,"toto");
    }
    public function testCreerPublicationTropGrande(){
        $this->expectException(ServiceException::class);
        $u = $this->createMock(Utilisateur::class);
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->withAnyParameters()->willReturn($u);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication(1,str_repeat("chaine", 300));
    }

    /**
     * @throws \Exception
     */
    public function testNombrePublications(){
        //Fausses publications, vides
        $fakePublications = [new Publication(), new Publication()];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recuperer")->willReturn($fakePublications);
        //Test
        $this->assertCount(2, $this->service->recupererPublications());
    }

    public function testNombrePublicationsUtilisateur(){
        $fakePublications = [new Publication()];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recupererParAuteur")->with(1)->willReturn($fakePublications);
        $this->assertEquals(1,count($this->service->recupererPublicationsUtilisateur(1)));
    }
    public function testNombrePublicationsUtilisateurInexistant(){
        $fakePublications = [];
        //On configure notre faux repository pour qu'il renvoie nos publications définies ci-dessus
        $this->publicationRepositoryMock->method("recupererParAuteur")->with(-1)->willReturn($fakePublications);
        $this->assertEquals(0,count($this->service->recupererPublicationsUtilisateur(-1)));
    }
    public function testRecupParUtilisateur(){
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Utilisateur inexistant.");
        count($this->service->recupererPublicationsUtilisateur(null));
    }
}