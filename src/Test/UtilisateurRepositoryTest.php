<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesI;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryI;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryI $utilisateurRepository;

    private static ConnexionBaseDeDonneesI $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    public function testSimpleNombreUtilisateurs() {
        $this->assertEquals(2, count(self::$utilisateurRepository->recuperer()));
    }
    public function testRecupParId() {
        $this->assertEquals(1, self::$utilisateurRepository->recupererParClePrimaire(1)->getIdUtilisateur());
    }
    public function testRecupParLogin() {
        $this->assertEquals(1, self::$utilisateurRepository->recupererParLogin("test")->getIdUtilisateur());
    }

    public function testRecupParEmail() {
        $this->assertEquals("test", self::$utilisateurRepository->recupererParEmail("test@example.com")->getLogin());
    }
    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}
