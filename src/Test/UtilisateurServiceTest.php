<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\Repository\UtilisateurRepositoryI;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

    //Dossier où seront déplacés les fichiers pendant les tests
    private  $dossierPhotoDeProfil = __DIR__."/tmp/";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = /* TODO */
        $this->fileMovingService = /* TODO */
            mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService(/* TODO */);
    }

    public function testCreerUtilisateurPhotoDeProfil() {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
            /* TODO : Tester l'existence du fichier (et eventuellement d'autres tests) */
        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach(scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil.$file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}